
import sys
import os
import logging
import quart
import tinydb
import asyncio
import time
import signal

import discordhost
import config

app_version = "1.0beta"

app = quart.Quart(__name__)
app.config["DEBUG"] = config.debug
app.config["MAX_CONTENT_LENGTH"] = config.max_content_size

db = tinydb.TinyDB(os.path.join(os.path.abspath(os.path.dirname(__file__)), "users.json"))
discord = None

@app.before_serving
async def before_serving():
    loop = asyncio.get_event_loop()

    global discord
    discord = discordhost.DiscordHost(db)
    
    await discord.login(config.discord_api_key)

    loop.create_task(discord.connect())

@app.route("/", methods=["GET"])
async def root():
    return await quart.render_template("index.html")

@app.route("/download/<path:filename>", methods = ["GET"])
async def download(filename):
    return await quart.send_from_directory(files_dir(), filename)

@app.route("/submit", methods=["POST"])
async def submit():
    quart.request.body_timeout = config.request_timeout

    request_form = await quart.request.form

    request_metadata = request_form.to_dict(flat=True)

    file_name = request_metadata["file_name"].replace(" ", "_")
    guild_label = request_metadata["guild_label"]
    user_id = request_metadata["user_id"]

    results = db.search(tinydb.Query().id == user_id)
    if len(results) < 1:
        return quart.jsonify({
            "error": "User {0} doesn't exist!".format(user_id)
        }), 400

    user_name = results[0].get("name")
    files = results[0].get("files")

    request_files = await quart.request.files
    file = request_files["file"]

    upload_folder = os.path.join(files_dir(), user_id)
    os.makedirs(upload_folder, exist_ok=True)

    file_path = os.path.join(upload_folder, file_name)

    await file.save(file_path)

    file.seek(0, os.SEEK_END)
    file_length = file.tell()

    new_file = True
    for file in files:
        if file.get("filename") == file_name:
            file["filesize"] = file_length
            file["date"] = int(time.time() * 1000)

            new_file = False
            break

    if new_file:
        files.append({
            "filename": file_name,
            "filesize": file_length,
            "date": int(time.time() * 1000)
        })

    db.update({"files": files}, tinydb.Query().id == user_id)

    notify_guild = config.notify.get(guild_label)

    if notify_guild is not None and notify_guild.get("notify"):
        for guild in discord.guilds:
            if guild.get_member(int(user_id)) is not None:
                if notify_guild.get("guild_name") == guild.name:
                    for channel in guild.channels:
                        if channel.name in notify_guild.get("channels"):
                            url = config.web_url + "/download/" + user_id + "/" + file_name
                            message = None

                            if new_file:
                                message = "User {0} has uploaded a file: {1}".format(user_name, url)
                            else:
                                message = "User {0} has uploaded a new version of file: {1}".format(user_name, url)

                            await channel.send(message)

    return await quart.render_template("index.html")

@app.route("/auth", methods=["POST"])
async def auth():
    form = await quart.request.form
    secretKey = form["secretKey"]

    user = tinydb.Query()
    results = db.search(user.key == secretKey)

    if len(results) > 0:
        user = results[0]
        user.pop("key", None)
        return quart.jsonify(user)
    else:
        return quart.jsonify({
            "error": "User with secret key {0} doesn't exist!".format(secretKey)
        }), 400

@app.route("/getfiles", methods=["GET"])
async def get_files():
    request_metadata = quart.request.args.to_dict(flat = True)
    user_id = request_metadata["user_id"]

    results = db.search(tinydb.Query().id == user_id)
    if len(results) < 1:
        return quart.jsonify({
            "error": "User {0} doesn't exist!".format(user_id)
        }), 400

    files = results[0].get("files")
    return quart.jsonify(files)

@app.route("/deletefile", methods=["POST"])
async def delete_file():
    request_form = await quart.request.form
    request_metadata = request_form.to_dict(flat = True)

    user_id = request_metadata["user_id"]
    file_name = request_metadata["file_name"]

    results = db.search(tinydb.Query().id == user_id)
    if len(results) < 1:
        return quart.jsonify({
            "error": "User {0} doesn't exist!".format(user_id)
        }), 400

    files = results[0].get("files")

    file_exists = False
    for file in files:
        if file.get("filename") == file_name:
            file_exists = True
            files.remove(file)

            db.update({"files": files}, tinydb.Query().id == user_id)
            break

    if not file_exists:
        return quart.jsonify({
            "error": "File {0} of user {1} doesn't exist in DB!".format(file_name, user_id)
        }), 400

    upload_folder = os.path.join(files_dir(), user_id)
    file_path = os.path.join(upload_folder, file_name)

    if os.path.exists(file_path):
        os.remove(file_path)
        return quart.jsonify({
            "error": "Success."
        }), 200
    else:
        return quart.jsonify({
            "error": "File {0} of user {1} doesn't exist on disk!".format(file_name, user_id)
        }), 400

@app.route("/getnotify", methods=["GET"])
async def get_notify():
    request_metadata = quart.request.args.to_dict(flat = True)
    user_id = request_metadata["user_id"]

    results = db.search(tinydb.Query().id == user_id)
    if len(results) < 1:
        return quart.jsonify({
            "error": "User {0} doesn't exist!".format(user_id)
        }), 400

    notify = {}

    for guild in discord.guilds:
        for guild_label in config.notify:
            guild_name = config.notify[guild_label].get("guild_name")

            if guild.get_member(int(user_id)) is not None and guild_label not in notify and guild_name == guild.name:
                notify[guild_label] = config.notify[guild_label].copy()

    return quart.jsonify({
        "notify": notify
    })


def files_dir():
    return os.path.join(os.path.abspath(os.path.dirname(__file__)), "files")

def signal_handler(sig, frame):
    db.close()
    sys.exit(0)

if __name__ == "__main__":
    logging.basicConfig(stream=sys.stdout, level=logging.INFO, datefmt="[%Y-%m-%d %H:%M:%S]", format="%(asctime)s %(levelname)s %(message)s")
    logging.getLogger("requests").setLevel(logging.WARNING)
    logging.getLogger("urllib3").setLevel(logging.WARNING)

    logging.info("Starting Discord File Bot (" + app_version + ")")

    signal.signal(signal.SIGINT, signal_handler)

    app.run(host=config.app_host, port=config.app_port)