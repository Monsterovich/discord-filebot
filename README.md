# Requirments:

- [Python 3.8](https://www.python.org/downloads/)
- Open admin terminal (Windows: right click cmd.exe -> "Run as administrator")
- Type the command `pip3 install -r requirments.txt`
- Copy `config.py.default` to `config.py` and change the settings.

# How to use:
- Create a bot by using [Discord Developer Portal](https://discord.com/developers/applications).
- Go to `Bot -> Server Members Intent` and enable it. This setting is mandatory, otherwise your bot won't be able to see other users on the server and will work incorrectly.
- You should invite a bot to the server. To do so, use this link `https://discord.com/oauth2/authorize?client_id=BOT_CLIENT_ID&permissions=0&scope=bot` where BOT_CLIENT_ID is your bot's client ID (see `OAuth2`).
- In order to login in into the bot's web application, you will need a secret key which you can obtain by PMing a bot with `$register` command. 

