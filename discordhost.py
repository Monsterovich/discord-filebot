#!/usr/bin/python3

import discord
import tinydb

import secrets
import logging

class DiscordHost(discord.Client):
    def __init__(self, db):
        intents = discord.Intents.default()
        intents.members = True
        super().__init__(intents=intents)

        self.db = db

    async def on_ready(self):
        logging.info("Discord bot connected.")

    async def on_message(self, message):
        if message.author.bot:
            return

        if message.channel.type == discord.ChannelType.private:
            if message.content.startswith("$register"):
                user = tinydb.Query()
                results = self.db.search(user.id == str(message.author.id))

                if len(results) > 0:
                    await message.reply("This id already exists.")
                    return

                secret_key = secrets.token_hex(16)
                self.db.insert({
                    "name": message.author.name, 
                    "id": str(message.author.id),
                    "key": secret_key,
                    "files": []
                })

                await message.channel.send("A new secret key has been registered: " + secret_key)

